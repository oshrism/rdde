# -*- coding: utf-8 -*-
"""
Created on Sun Jun 26 10:54:54 2016

@author: oshrib
"""

import numpy as np 
import re 
import weka.core.jvm as jvm 
from weka.classifiers import Classifier
from weka.core.dataset import Attribute
from weka.core.dataset import Instances , Instance
from weka.core.converters import Loader
from sklearn.metrics import mean_squared_error
from math import sqrt
import os 




def RMS(predicted,actual):
    return sqrt(mean_squared_error(actual,predicted))

def create_instances_from_lists_nominal(x, y,attr, name="data"):
    """
    Allows the generation of an Instances object from a list of lists for X and a list for Y.
    All data must be numerical. Attributes can be converted to nominal with the
    weka.filters.unsupervised.attribute.NumericToNominal filter.

    :param x: the input variables (row wise)
    :type x: list of list
    :param y: the output variable
    :type y: list
    :param name: the name of the dataset
    :type name: str
    :return: the generated dataset
    :rtype: Instances
    """
    
    if len(x) != len(y):
        raise Exception("Dimensions of x and y differ: " + str(len(x)) + " != " + str(len(y)))
    # create header
    atts = []
    for i in xrange(len(x[0])):
        atts.append(Attribute.create_nominal("x" + str(i+1),attr))
    atts.append(Attribute.create_nominal("y",attr))
    result = Instances.create_instances(name, atts, len(y))
    # add data
    for i in xrange(len(x)):
        values = x[i][:]
        values.append(y[i])
        result.add_instance(Instance.create_instance(values))
    return result
    
def create_instances_from_lists_nominal_file(x,y,attr,name="data"):
    f = file ("temp.arff",'w')
    f.write("@relation " + name +"\n")
    atts = ' {'+','.join(attr)+'}'
    for i in range(len(x[0])):
        f.write('@attribute x' + str(i) + atts +"\n")
    f.write('@attribute y ' + atts +"\n")
    f.write('@data\n')
    
    for i in range(len(x)):
        values = list(x[i][:])
        values.append(y[i])
        f.write(','.join(str(int(j)) for j in values) + '\n')
    f.close()
    loader = Loader(classname="weka.core.converters.ArffLoader")
    data = loader.load_file('temp.arff')
    return data
    
    

class OLM(object):
    
    def __init__(self,vector,target,attr=['0','1','2','3','4'],options=["-R","2","-C","1","-U", "10"]):
#        jvm.start(packages=True)
        self.cls = Classifier(classname="weka.classifiers.rules.OLM", options=options)
        #loader = Loader(classname="weka.core.converters.ArffLoader")
        #self.data = loader.load_file(filePath) 
        if not (type(target) == list):         
            target = [target]*(len(vector))            
        self.data = create_instances_from_lists_nominal_file(vector,target,attr)
        self.data.class_is_last()
        self.cls.build_classifier(self.data)

    # find all get_rules and replace     
    def get_rules_with_traget(self):
        #get the rules from the cls string 
        rules = np.array([np.array([int (x) for x in y.split(',')] )for y in re.findall(r'[\d+,]+\d+', str(self.cls))])
        #seprid between the rules and the targed 
        rules = [(x[:-1],x[-1]) for x in rules]
        return rules
    
    def get_rules(self):
        #get the rules from the cls string 
        rules = np.array([np.array([int (x) for x in y.split(',')] )for y in re.findall(r'[-*\d+,]+\d+', str(self.cls))])
        #seprid between the rules and the targed 
        rules = [x[:-1] for x in rules]
        return rules
        
    def get_label(self):
        lable = []
        rules = self.get_rules()
        for i in range(self.data.num_instances):
            z = self.data.get_instance(i)
            vector = np.array([int(x) for x in str(z).split(',')[:-1]])
            min_rms = 100
            min_idx = 0 
            for idx, r in enumerate(rules):
                rms = RMS(r,vector)
                if rms < min_rms:
                    min_rms = rms
                    min_idx = idx
            lable.append(min_idx)
        return lable    
        
                

