# -*- coding: utf-8 -*-
"""
Created on Mon Jun 13 13:41:46 2016

@author: oshrib
"""
import numpy as np
from sklearn.cluster import KMeans

def createRules(Data,y):
    weight = np.linalg.lstsq(Data,y)[0]
    k = KMeans(n_clusters = 10)
    k = k.fit(Data)
    label = np.dot(k.cluster_centers_,weight)
    label = [round(x) for x in label]
    
    
    
    