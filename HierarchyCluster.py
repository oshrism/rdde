# -*- coding: utf-8 -*-
"""
Created on 23/10/2016.
runfile('T:/src/HierarchyCluster.py',wdir='T:/')
@author: oshrib
"""

from sklearn.cluster.hierarchical import AgglomerativeClustering
import pandas as pd


class HierarchyCluster(object):

    def __init__(self, n_clusters):
        self.n_clusters = n_clusters

    def fit(self, data):
        model = AgglomerativeClustering(n_clusters=self.n_clusters)
        self.labels_ = model.fit_predict(data)
        df = pd.DataFrame(data)
        df['c'] = self.labels_
        self.cluster_centers_ = df.groupby('c').mean().sort_index().values
        return self
