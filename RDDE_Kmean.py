# -*- coding: utf-8 -*-
"""
Created on Mon Jun 06 12:37:21 2016

@author: oshrib
"""

from __future__ import print_function
from __future__ import division
import numpy as np
import sys

if len(sys.argv[1]) == 1 or sys.argv[1].lower() == 'kmeans':
    from sklearn.cluster import KMeans as cluster_alguritam
if sys.argv[1].lower() == 'hierarchy':
    from HierarchyCluster import HierarchyCluster as cluster_alguritam

befor_point = 3
after_point = 3


def float_kind(x):
    return "%{0}.{1}f".format(befor_point, after_point) % x


# TODO wrap the k-mean class
# TODO change the fit function to create mass

class RDDE(object):
    def __init__(self, rules, init_buffers, parm, logger, output):
        Buffer.output = output
        global befor_point, after_point
        befor_point = parm.befor_point
        after_point = parm.after_point
        self.resultPath = parm.resultPath
        self.logger = logger
        self.delta = parm.delta
        self.splitNumber = parm.splitNumber
        self.maxLenBuffer = parm.bufferSize  # TODO change max len buffer to buffer size
        self.differences = parm.differences
        self.minBufferLenToSplit = parm.minBufferLenToSplit
        self.maxBuffersCreat = parm.maxBuffersCreat
        self.updataBufferMethodOption = parm.updataBufferMethodOption
        self.buffers = []
        self.new_rules_counter = len(rules)
        for idx, (r, b) in enumerate(zip(rules, init_buffers)):
            self.buffers.append(Buffer(self.maxLenBuffer, massVector=r, class_name=idx, buf=b,
                                       updataBufferMethodOption=self.updataBufferMethodOption))  # ,buf = r,counter = 1

    def __iter__(self):
        return iter(self.buffers)

    def print_buffer_status(self):
        with open(self.resultPath, 'w') as w:
            w.write(','.join(['buffer_name,status,buffer_len,vector']) + '\n')
            for buf in self.buffers:
                arr2str = np.array2string(buf.VMass, formatter={'float_kind': float_kind})
                w.write(','.join([buf.class_name, '1', str(len(buf)), arr2str]) + '\n')

    def addVectorToClustering(self, vector):
        minDist = 1000
        minClu = 0
        rules = []  # for debug
        # the loop lock for the best RSME
        for idx, buff in enumerate(self.buffers):
            dis = self.differences(vector, buff.VMass)
            rules.append(buff.VMass)
            if dis < minDist:
                minDist = dis
                minClu = idx
        # add vector to buffer

        if (minDist < self.delta) or (len(self.buffers) >= self.maxBuffersCreat and self.maxBuffersCreat != -1):
            closest_rule = self.buffers[minClu].class_name
            self.buffers[minClu].addVector(vector, self.updataBufferMethodOption)
            self.logger.info('+++++Begin Add Vector+++')
            self.logger.info(
                'Add vector to cluster: {}, The cluster count is: {}'.format(self.buffers[minClu].class_name,
                                                                             self.buffers[minClu].counter))
            self.logger.info('The vector is: {}'.format(np.array2string(vector, formatter={'float_kind': float_kind})))
            self.logger.info('The rule of cluster #{0} is: {1} '.format(self.buffers[minClu].class_name,
                                                                        np.array2string(
                                                                            formatter={'float_kind': float_kind},
                                                                            a=self.buffers[minClu].VMass)))
            self.logger.info('The delta is: {0} The differences is: {1} '.format(self.delta, minDist))
            self.logger.info('+++++End Add Vector++++')
        else:  # split or add  buffer
            closest_rule = self.splitOrAddBuffer(minClu, vector, minDist)

        self.logger.info('The number of the buffers after added the vector is: {}'.format(len(self.buffers)))
        for buff in self.buffers:
            if buff.class_name == closest_rule:
                diff = self.differences(vector, buff.VMass)
                buffer_len = len(buff.buffer)
                cluster_count = buff.counter
                break

        return diff, closest_rule, buffer_len, cluster_count, len(self.buffers)

    def splitOrAddBuffer(self, minClu, vector, minDist):
        currentsplit = 2
        stopSpliting = False
        minIdx = 0
        k = None
        self.logger.info('+++++++Begin split OR Add Buffer++++++')
        self.logger.info('The vector is: {}'.format(np.array2string(vector, formatter={'float_kind': float_kind})))
        self.logger.info('The closest rule is: {}'.format(
            np.array2string(self.buffers[minClu].VMass, formatter={'float_kind': float_kind})))
        self.logger.info('The delta is: {0} The differences is: {1}'.format(self.delta, minDist))
        # the loop looks for an option to split the nearest buffer 
        maxLenBuffertemp = len(self.buffers[minClu])
        if self.maxBuffersCreat != -1 and (self.maxBuffersCreat - len(self.buffers)) < self.splitNumber:
            splitNumber = self.maxBuffersCreat - len(self.buffers)
        else:
            splitNumber = self.splitNumber
        while (currentsplit <= splitNumber) and (not stopSpliting) and (maxLenBuffertemp > self.minBufferLenToSplit):
            self.logger.info('********Begin split to {0} Buffers*********'.format(currentsplit))
            k = cluster_alguritam(n_clusters=currentsplit)
            k = k.fit(self.buffers[minClu].buffer)
            for idx, VMass in enumerate(k.cluster_centers_):
                dis = self.differences(VMass, vector)
                if dis < minDist:
                    minDist = dis
                    minIdx = idx
            if minDist < self.delta:
                stopSpliting = True
            self.logger.info('-----Begin print Buffer --- ')
            for cl, vec in enumerate(k.cluster_centers_):
                self.logger.info('Clusert Number {0} Vecotr {1}'.format(cl, np.array2string(vec, formatter={
                    'float_kind': float_kind})))
            self.logger.info('-----End print Buffer ---')
            self.logger.info('The closest split cluster {0} rule vector {1}'.format(minIdx, np.array2string(
                k.cluster_centers_[minIdx], formatter={'float_kind': float_kind})))
            self.logger.info('The delta is: {0} The differences is: {1} Can is split: {2} '.format(self.delta, minDist,
                                                                                                   stopSpliting))
            self.logger.info('********End split to {0} Buffers*********'.format(currentsplit))
            currentsplit += 1
        if stopSpliting:  # thire have an option to split the buffer
            self.logger.info('#########begin split #####')
            self.logger.info(
                "split to {0} buffers The delta is: {1} The closet differences is: {2} The vector {3}".format(
                    currentsplit - 1, self.delta, minDist,
                    np.array2string(vector, formatter={'float_kind': float_kind})))
            self.logger.info('The closest split rule {}'.format(
                np.array2string(k.cluster_centers_[minIdx], formatter={'float_kind': float_kind})))
            closest_rule = self.splitBuffer(minClu, k, minIdx, vector)
            self.logger.info('#########End split #####')
        else:  # thire not have an option tosplit the buffer
            self.buffers.append(
                Buffer(self.maxLenBuffer, class_name=self.new_rules_counter, massVector=vector, buf=vector,
                       updataBufferMethodOption=self.updataBufferMethodOption))
            closest_rule = self.buffers[-1].class_name
            self.new_rules_counter += 1
            self.logger.info('########Add Buffer #####')
        self.logger.info('+++++++End split OR Add Buffer++++++')
        return closest_rule

    # TODO add the new parmter max buffer
    def splitBuffer(self, minClu, k, minIdx, vector):
        data = self.buffers[minClu].buffer
        extendBuffer = []
        class_name = self.buffers[minClu].class_name + '.'
        for idx, m in enumerate(k.cluster_centers_):
            extendBuffer.append(
                Buffer(self.maxLenBuffer, massVector=m, class_name=class_name + str(idx)))  # ,buf=m,counter=1
        pairDataLabels = zip(k.labels_, data)
        temp = 0
        for idx, v in pairDataLabels:
            extendBuffer[idx].addVector(v, self.updataBufferMethodOption)
            temp += 1
        self.logger.info(
            'The oldest counter is: {0} The oldest len buffer is: {1} '.format(self.buffers[minClu].counter,
                                                                               len(self.buffers[minClu].buffer)))
        for idx, b in enumerate(extendBuffer):
            b.updateCounter(self.buffers[minClu].counter, self.logger)
            self.logger.info('The update counter to buffer number {0} is: {1} '.format(idx, b.counter))
        extendBuffer[minIdx].addVector(vector, self.updataBufferMethodOption)  # add the real vector
        self.buffers.extend(extendBuffer)
        self.buffers.remove(self.buffers[minClu])
        return extendBuffer[minIdx].class_name


class Buffer(object):
    def __init__(self, lengthBuffer, class_name, massVector, buf=None, updataBufferMethodOption=None, bCalMass=False,
                 counter=0):
        self.maxLenBuffer = lengthBuffer
        self.VMass = massVector
        if buf is None:
            self.buffer = np.empty((0, len(massVector)))
            self.counter = counter
        elif updataBufferMethodOption is None:
            raise Exception('The buffer init is full but updateBufferMethodOption is None')
        else:
            self._init_buffer(buf, updataBufferMethodOption)

        self.bCalMass = bCalMass
        self.class_name = str(class_name)

    def _init_buffer(self, buf, updataBufferMethodOption):
        updataBufferMethodOption['massVector'] = self.VMass
        self.buffer = updataBufferMethodOption['initBufferFunction'](buf, updataBufferMethodOption)
        self.counter = len(self.buffer)

    def addVector(self, vector, updataBufferMethodOption):
        updataBufferMethodOption['massVector'] = self.VMass
        addToBufferFunction = updataBufferMethodOption['addToBufferFunction'](vector, updataBufferMethodOption)
        Buffer.output.info('The Vector {0} Assigned To {1:5} Add To Buffer: {2}'.format(
            np.array2string(vector, formatter={'float_kind': float_kind}), self.class_name, addToBufferFunction))

        # TODO if the method is set edge cases, out the weak vector
        if addToBufferFunction:
            self.buffer = np.vstack((self.buffer, vector))
            if self.maxLenBuffer < len(self.buffer):
                self.buffer = self.buffer[1:]
            if self.bCalMass:
                self.VMass = np.mean(self.buffer, axis=0)
        self.counter += 1

    def calMass(self):
        if self.bCalMass:
            self.VMass = np.mean(self.buffer, axis=0)

    def updateCounter(self, lastCounter, logger=None):
        """
        Update the number of vectors in a buffer in case the buffer splits.
        Assign to this buffer the proportional number of vectors from the "root" buffer.
        :param lastCounter:
        :param logger:
        :return:
        """
        den = min(lastCounter, self.maxLenBuffer)
        ratio = len(self.buffer) / den
        self.counter = np.round(lastCounter * ratio)
        if logger is not None:
            buffer_len = len(self.buffer)
            logger.info('The den is {0} the ratio is: {1}, '
                        'the len buffer is: {2} and the new counter is: {3}'.format(den, ratio, buffer_len,
                                                                                    self.counter))
            logger.info('~~~~~~~~~~Begin print buffer~~~~~~~~~~~~~~~~~')
            for v in self.buffer:
                logger.info(np.array2string(v, formatter={'float_kind': float_kind}))
            logger.info('~~~~~~~~~~End print buffer~~~~~~~~~~~~~~~~~')

    def __len__(self):
        if self.buffer is None:
            return 0
        if type(self.buffer[0]) == np.ndarray:
            return len(self.buffer)
        return 1
