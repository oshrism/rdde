# -*- coding: utf-8 -*-
"""
Created on Thu Jun 23 11:36:39 2016

@author: oshrib
"""

import ConfigParser
from sklearn.metrics import mean_squared_error
from math import sqrt
import numpy as np
import os


def fifo(vector, option):
    return True


# TODO change the remove vector from the buffer
def setEdgeCases(vector, option):
    massVector = option['massVector']
    deltaOut = option['deltaOut']
    deltaIn = option['deltaIn']
    differences = option['differences']
    diff = differences(vector, massVector)
    if (diff > deltaIn) and (diff < deltaOut):
        return True
    return False


def init_buffer_setEdgeCases(vectors, option):
    diff_vector = np.apply_along_axis(option['differences'], 1, vectors, args=(option['massVector']))
    idxs = np.where((diff_vector <= option['deltaOut']) & (diff_vector >= option['deltaIn']))[:option['bufferSize']]
    return vectors[idxs]


def init_buffer_fifo(vectors, option):
    return np.array(vectors[:option['bufferSize']])


def RMS(predicted, actual):
    return sqrt(mean_squared_error(actual, predicted))


class Parameter(object):
    def __init__(self, FilePath):
        self.config = ConfigParser.ConfigParser()
        self.config.read(FilePath)

    def LoadParam(self):
        addToBufferFunctionMap = dict(Fifo=fifo, setEdgeCases=setEdgeCases)
        initBufferFunctionMpa = dict(Fifo=init_buffer_fifo, setEdgeCases=init_buffer_setEdgeCases)
        differencesMap = dict(RMS=RMS)

        # Get regular parameter

        self.differences = differencesMap[self.config.get('Parameter', 'Differences')]
        self.bufferSize = int(self.config.get('Parameter', 'BufferSize'))
        self.delta = float(self.config.get('Parameter', 'Delta'))
        self.splitNumber = int(self.config.get('Parameter', 'SplitNumber'))
        self.minBufferLenToSplit = int(self.config.get('Parameter', 'minBufferLenToSplit'))
        self.StartNumberBuffers = int(self.config.get('Parameter', 'StartNumberBuffers'))
        self.maxBuffersCreat = int(self.config.get('Parameter', 'MaxNumberOfBuffers'))
        self.weightVector = np.array([float(x) for x in self.config.get('Parameter', 'WeightVector').split(',')])
        seed = self.config.get('Parameter', 'Seed')  # type: str
        self.seed = int(seed) if seed.isdigit() else None

        # Get Excel parameter

        self.excel_path = self.config.get('Excel', 'Path')
        self.sheet = self.config.get('Excel', 'Sheet')
        self.from_row = int(self.config.get('Excel', 'FromRow'))
        self.until_row = int(self.config.get('Excel', 'UntilRow'))

        # Get updata buffer method paramter 
        self.addToBufferFunction = addToBufferFunctionMap[self.config.get('UpdataBufferMethod', 'Function')]
        self.initBufferFunction = initBufferFunctionMpa[self.config.get('UpdataBufferMethod', 'Function')]
        self.updataBufferMethodOption = {}
        self.updataBufferMethodOption['deltaOut'] = self.delta
        # TODO a difference function to defferences-update-buffer-paramter
        self.updataBufferMethodOption['differences'] = self.differences
        self.updataBufferMethodOption['addToBufferFunction'] = self.addToBufferFunction
        self.updataBufferMethodOption['initBufferFunction'] = self.initBufferFunction
        self.updataBufferMethodOption['deltaIn'] = self.config.get('UpdataBufferMethod', 'EdgeDltaIn')
        self.updataBufferMethodOption['bufferSize'] = self.bufferSize
        # Get Debug paramter
        self.debugMode = bool(self.config.get('Debug', 'DebugMode'))
        debugPath = self.config.get('Debug', 'FilePath')
        if debugPath == '':
            debugPath = 'debug.txt'
        else:
            try:
                open(debugPath, 'w').close()
                os.remove(debugPath)
            except Exception as e:
                print (e.message)
                debugPath = 'debug.txt'
        self.debugPath = debugPath
        self.befor_point = int(self.config.get('Debug', 'BeforPoint'))
        self.after_point = int(self.config.get('Debug', 'AfterPoint'))

        # Get Result path paramter
        resultPath = self.config.get('Parameter', 'ResultPath')
        if resultPath == '':
            resultPath = 'Result_output.csv'
        else:
            try:
                open(resultPath, 'w').close()
                os.remove(resultPath)
            except Exception as e:
                print (e.message)
                resultPath = 'Result_output.csv'
        self.resultPath = resultPath
