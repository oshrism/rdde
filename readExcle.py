# -*- coding: utf-8 -*-
"""
Created on Sun Jun 05 14:03:55 2016

@author: oshrib
"""

import numpy as np
# import xlsxwriter
import xlrd
import os


class readExcle(object):
    def __init__(self, path, sheet_name):
        if not (os.path.exists(path)):
            raise Exception('The file {0} doesn\'t exists'.format(path))
        self.workbook = xlrd.open_workbook(path)
        self.worksheet = self.workbook.sheet_by_name(sheet_name)
        self.pointerToRow = 701
        self.nRows = self.worksheet.nrows
        self.nCols = self.worksheet.ncols

    def readRow(self):
        if (self.pointerToRow == self.nRows):
            return None  # error
        data = np.array([self.worksheet.cell_value(self.pointerToRow, cols) for cols in range(1, self.nCols)])
        self.pointerToRow += 1
        return data

    def readAllData(self, fromRow=-1, untileRow=-1):
        if (untileRow == -1):
            untileRow = self.nRows
        data = np.array(
            [[float(self.worksheet.cell_value(r, c)) for c in range(0, self.nCols)] for r in range(fromRow, untileRow)],
            dtype=np.float64)
        return data
