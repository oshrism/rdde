# -*- coding: utf-8 -*-
"""
Created on Sun Jun 05 16:11:08 2016
runfile('T:/src/main.py',wdir='T:/wd')
@author: oshrib
#"""
from __future__ import division
from readExcle import readExcle
from sklearn.metrics import mean_squared_error
from math import sqrt
from RDDE_Kmean import RDDE
from Parameter import Parameter
import logging
import numpy as np

import sys

if len(sys.argv[1]) == 1 or sys.argv[1].lower() == 'kmeans':
    from sklearn.cluster import KMeans as cluster_alguritam
elif sys.argv[1].lower() == 'hierarchy':
    from HierarchyCluster import HierarchyCluster as cluster_alguritam
else:
    print('You must choose KMean or Hierarchy, if empty the default is KMeans')


def fifo(vector, option):
    return True


def setEdgeCases(vector, option):
    massVector = option['massVector']
    deltaOut = option['deltaOut']
    deltaIn = option['deltaIn']
    differences = option['differences']
    diff = differences(vector, massVector)
    if ((diff > deltaIn) and (diff < deltaOut)):
        return True
    return False


def RMS(predicted, actual):
    return sqrt(mean_squared_error(actual, predicted))


def get_number(l):
    number = 0
    for x in l:
        number = number * 10
        number += x
    return number


# parameter
param = Parameter('Parameter.ini')

param.LoadParam()

np.random.seed(param.seed)
bufferSize = param.bufferSize
delta = param.delta
splitNumber = param.splitNumber
minBufferLenToSplit = param.minBufferLenToSplit
addToBufferOption = dict(deltaOut=delta, deltaIn=param.updataBufferMethodOption['deltaIn'],
                         differences=param.differences, addToBufferFunction=param.addToBufferFunction)
weightVector = param.weightVector

print (__name__)
# ===================logger for debug========================================
logger = logging.getLogger(__name__)
fh = logging.FileHandler(filename=param.debugPath, mode='w')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('Time: %(asctime)s %(funcName)-21s(): %(message)s', '%H:%M:%S')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
logger.addHandler(fh)
logger.addHandler(ch)

if param.debugMode:
    logger.setLevel(logging.DEBUG)
else:
    pass
# =================logger for result=======================================

result = logging.Logger('result')
fileResult = logging.FileHandler(filename=param.resultPath, mode='w')
fileResult.setLevel(logging.INFO)
fileResult.setFormatter(formatter)
result.addHandler(fileResult)

# ==============================================================================
wb = readExcle(param.excel_path, param.sheet)
data = wb.readAllData(fromRow=param.from_row, untileRow=param.until_row)
vector = [x[1:-1].tolist() for x in data]
target = [x[-1] for x in data]
k = cluster_alguritam(n_clusters=param.StartNumberBuffers)
k.fit(vector)
rules = k.cluster_centers_

# ==========init first buffers=========================
init_buffer = [[] for _ in range(param.StartNumberBuffers)]
for label, vec in zip(k.labels_, vector):
    init_buffer[label].append(vec)
# ==============================================================================


b = RDDE(rules, parm=param, logger=logger, output=result, init_buffers=init_buffer)

data = wb.readAllData(fromRow=param.until_row)
data = [(x[1:-1] * weightVector, x[-1]) for x in data]

df = []
for d in data:
    df.append(b.addVectorToClustering(d[0]))

import pandas as pd

pd.DataFrame(df, columns=['Differences', 'Number of closest rule', 'Len buffer', 'Cluster count',
                          'Len buffer after added']).to_csv('results.csv')

b.print_buffer_status()

print ('End')
